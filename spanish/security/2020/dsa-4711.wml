#use wml::debian::translation-check translation="3ce93908ff9ed5d5edeae5cc5bcfa241d553b2f8"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en coturn, un servidor TURN y
STUN para VoIP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4067">CVE-2020-4067</a>

    <p>Felix Doerre informó de que el área de memoria de respuesta STUN no se inicializaba
    correctamente, lo que podía permitir que un atacante filtrara bytes procedentes de la
    conexión de otro cliente en los bytes de relleno.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6061">CVE-2020-6061</a>

    <p>Aleksandar Nikolic informó de que una petición HTTP POST manipulada podía
    dar lugar a fugas de información y a otros comportamientos anómalos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6062">CVE-2020-6062</a>

    <p>Aleksandar Nikolic informó de que una petición HTTP POST manipulada podía
    dar lugar a la caída del servidor y a denegación de servicio.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 4.5.0.5-1+deb9u2.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 4.5.1.1-1.1+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de coturn.</p>

<p>Para información detallada sobre el estado de seguridad de coturn, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/coturn">https://security-tracker.debian.org/tracker/coturn</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4711.data"
