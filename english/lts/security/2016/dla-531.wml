<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been found in the Simple Protocol for Independent Computing
Environments, SPICE.</p>

<p>Frediano Ziglio from Red Hat discovered that SPICE allowed local guest
OS users to read from or write to arbitrary host memory locations via
crafted primary surface parameters.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem has been fixed in version 0.11.0-1+deb7u3.</p>

<p>We recommend you to upgrade your spice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-531.data"
# $Id: $
