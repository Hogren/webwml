<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a remote denial-of-service (DoS) vulnerability
in memcached, a high-performance memory object caching system.</p>

<p>The try_read_command function allowed remote attackers to cause a DoS via a
request to add/set a key that makes a comparison between a signed and unsigned
integer which triggered a heap-based buffer over-read.</p>

<p>This vulnerability existed due to an incomplete upstream fix for <a href="https://security-tracker.debian.org/tracker/CVE-2016-8705">CVE-2016-8705</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in memcached version
1.4.13-0.2+deb7u3.</p>

<p>We recommend that you upgrade your memcached packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1033.data"
# $Id: $
