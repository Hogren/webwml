<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA-1025-1 in bind9 introduced a
regression.</p>

<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2017-3142">CVE-2017-3142</a> 
broke verification of TSIG signed TCP message
sequences where not all the messages contain TSIG records.
This is conform to the spec and may be used in AXFR and IXFR response.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:9.8.4.dfsg.P1-6+nmu2+deb7u18.</p>

<p>We recommend that you upgrade your bind9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1025-2.data"
# $Id: $
