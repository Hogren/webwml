<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Charles A. Roelli discovered that Emacs is vulnerable to arbitrary code
execution when rendering text/enriched MIME data (e.g. when using
Emacs-based mail clients).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
23.4+1-4+deb7u1.</p>

<p>We recommend that you upgrade your emacs23 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1101.data"
# $Id: $
