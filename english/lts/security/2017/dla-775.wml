<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-0839">CVE-2015-0839</a>

  <p>The hplip plugin download function verifies the driver using a
  short-key. This is not secure because it is trivial to
  generate keys with arbitrary key IDs.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.12.6-3.1+deb7u2.</p>

<p>We recommend that you upgrade your hplip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-775.data"
# $Id: $
