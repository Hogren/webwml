<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Out-of-bounds read and write conditions have been fixed in clamav.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1787">CVE-2019-1787</a>

    <p>An out-of-bounds heap read condition may occur when scanning PDF
    documents. The defect is a failure to correctly keep track of the number
    of bytes remaining in a buffer when indexing file data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1788">CVE-2019-1788</a>

    <p>An out-of-bounds heap write condition may occur when scanning OLE2 files
    such as Microsoft Office 97-2003 documents. The invalid write happens when
    an invalid pointer is mistakenly used to initialize a 32bit integer to
    zero. This is likely to crash the application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1789">CVE-2019-1789</a>

    <p>An out-of-bounds heap read condition may occur when scanning PE files
    (i.e. Windows EXE and DLL files) that have been packed using Aspack as a
    result of inadequate bound-checking.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.100.3+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1759.data"
# $Id: $
