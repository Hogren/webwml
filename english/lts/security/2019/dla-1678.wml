<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues have been found in the Thunderbird mail client,
which could lead to the execution of arbitrary code, denial of service
or spoofing of S/MIME signatures.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:60.5.1-1~deb8u1.</p>

<p>We recommend that you upgrade your thunderbird packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1678.data"
# $Id: $
