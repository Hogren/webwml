<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A Polymorphic Typing issue was discovered in jackson-databind, a JSON
library for Java. When Default Typing is enabled (either globally or
for a specific property) for an externally exposed JSON endpoint, the
service has the mysql-connector-java jar (8.0.14 or earlier) in the
classpath, and an attacker can host a crafted MySQL server reachable
by the victim, an attacker can send a crafted JSON message that allows
them to read arbitrary local files on the server. This occurs because of
missing com.mysql.cj.jdbc.admin.MiniAdmin validation.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.4.2-2+deb8u6.</p>

<p>We recommend that you upgrade your jackson-databind packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1798.data"
# $Id: $
