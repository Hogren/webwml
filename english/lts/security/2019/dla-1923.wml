<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities were discovered in Ansible, a configuration
management, deployment, and task execution system.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3908">CVE-2015-3908</a>

    <p>A potential man-in-the-middle attack associated with insusfficient
    X.509 certificate verification.  Ansible did not verify that the
    server hostname matches a domain name in the subject's Common Name
    (CN) or subjectAltName field of the X.509 certificate, which allows
    man-in-the-middle attackers to spoof SSL servers via an arbitrary
    valid certificate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6240">CVE-2015-6240</a>

    <p>A symlink attack that allows local users to escape a restricted
    environment (chroot or jail) via a symlink attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10875">CVE-2018-10875</a>

    <p>A fix potential arbitrary code execution resulting from reading
    ansible.cfg from a world-writable current working directory.  This
    condition now causes ansible to emit a warning and ignore the
    ansible.cfg in the world-writable current working directory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10156">CVE-2019-10156</a>

    <p>Information disclosure through unexpected variable substitution.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.2+dfsg-2+deb8u2.</p>

<p>We recommend that you upgrade your ansible packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1923.data"
# $Id: $
