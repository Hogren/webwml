<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

    <p>When using Apache Tomcat and an attacker is able to control the
    contents and name of a file on the server; and b) the server is
    configured to use the PersistenceManager with a FileStore; and c)
    the PersistenceManager is configured with
    sessionAttributeValueClassNameFilter="null" (the default unless a
    SecurityManager is used) or a sufficiently lax filter to allow the
    attacker provided object to be deserialized; and d) the attacker
    knows the relative file path from the storage location used by
    FileStore to the file the attacker has control over; then, using a
    specifically crafted request, the attacker will be able to trigger
    remote code execution via deserialization of the file under their
    control. Note that all of conditions a) to d) must be true for the
    attack to succeed.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11996">CVE-2020-11996</a>

    <p>A specially crafted sequence of HTTP/2 requests sent to Apache
    Tomcat could trigger high CPU usage for several seconds. If a
    sufficient number of such requests were made on concurrent HTTP/2
    connections, the server could become unresponsive.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8.5.54-0+deb9u2.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>For the detailed security status of tomcat8 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat8">https://security-tracker.debian.org/tracker/tomcat8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2279.data"
# $Id: $
