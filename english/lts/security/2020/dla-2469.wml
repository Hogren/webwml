<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Some issues have been found in qemu, a fast processor emulator.</p>

<p>All issues are related to assertion failures, out-of-bounds access
failures or bad handling of return codes.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u12.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2469.data"
# $Id: $
