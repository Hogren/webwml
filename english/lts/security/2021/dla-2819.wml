<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in NTFS-3G, a read-write NTFS
driver for FUSE. A local user can take advantage of these flaws for
local root privilege escalation.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2016.2.22AR.1+dfsg-1+deb9u2.</p>

<p>We recommend that you upgrade your ntfs-3g packages.</p>

<p>For the detailed security status of ntfs-3g please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ntfs-3g">https://security-tracker.debian.org/tracker/ntfs-3g</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2819.data"
# $Id: $
