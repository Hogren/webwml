<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the OpenSC smart card utilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15945">CVE-2019-15945</a>

    <p>Out-of-bounds access of an ASN.1 Bitstring.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15946">CVE-2019-15946</a>

    <p>Out-of-bounds access of an ASN.1 Octet string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19479">CVE-2019-19479</a>

    <p>Incorrect read operation in the Setec driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26570">CVE-2020-26570</a>

    <p>Heap-based buffer overflow in the Oberthur driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26571">CVE-2020-26571</a>

    <p>Stack-based buffer overflow in the GPK driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26572">CVE-2020-26572</a>

    <p>Stack-based buffer overflow in the TCOS driver.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.16.0-3+deb9u2.</p>

<p>We recommend that you upgrade your opensc packages.</p>

<p>For the detailed security status of opensc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/opensc">https://security-tracker.debian.org/tracker/opensc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2832.data"
# $Id: $
