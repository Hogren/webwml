<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>PostgreSQL 9.6.23 fixes this security issue:</p>

  <p>Disallow SSL renegotiation more completely (Michael Paquier)</p>

  <p>SSL renegotiation has been disabled for some time, but the server would
  still cooperate with a client-initiated renegotiation request. A
  maliciously crafted renegotiation request could result in a server crash
  (see OpenSSL issue <a href="https://security-tracker.debian.org/tracker/CVE-2021-3449">CVE-2021-3449</a>).  Disable the feature altogether on
  OpenSSL versions that permit doing so, which are 1.1.0h and newer.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
9.6.23-0+deb9u1.</p>

<p>We recommend that you upgrade your postgresql-9.6 packages.</p>

<p>For the detailed security status of postgresql-9.6 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-9.6">https://security-tracker.debian.org/tracker/postgresql-9.6</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2751.data"
# $Id: $
