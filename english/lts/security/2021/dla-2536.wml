<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in libsdl2, a library for portable low
level access to a video framebuffer, audio output, mouse, and keyboard.
All issues are related to either buffer overflow, integer overflow or
heap-based buffer over-read, resulting in a DoS or remote code execution
by using crafted files of different formats.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.0.5+dfsg1-2+deb9u1.</p>

<p>We recommend that you upgrade your libsdl2 packages.</p>

<p>For the detailed security status of libsdl2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsdl2">https://security-tracker.debian.org/tracker/libsdl2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2536.data"
# $Id: $
