<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The p7zip package has a heap-based buffer overflow in the
NCompress::NShrink::CDecoder::CodeReal method in 7-Zip which allows
remote attackers to cause a denial of service (out-of-bounds write) or
potentially execute arbitrary code via a crafted ZIP archive.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
9.20.1~dfsg.1-4+deb7u3.</p>

<p>We recommend that you upgrade your p7zip packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1268.data"
# $Id: $
