<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Joran Herve discovered that the Okular document viewer was susceptible
to directory traversal via malformed .okular files (annotated document
archives), which could result in the creation of arbitrary files.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4:4.14.2-2+deb8u1.</p>

<p>We recommend that you upgrade your okular packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1516.data"
# $Id: $
