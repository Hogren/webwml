# Translation of date.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2011-02-13 18:35+0100\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: Slovak <debian-l10n-slovak@lists.debian.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "ned"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "pon"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "uto"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "str"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "štv"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "pia"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "sob"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "jan"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "feb"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "mar"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "apr"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "máj"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "jún"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "júl"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "aug"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "sep"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "okt"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "nov"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "dec"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "januára"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "februára"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "marca"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "apríla"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "<void id=\"fullname\" />mája"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "júna"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "júla"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "augusta"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "septembra"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "októbra"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "novembra"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "decembra"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]02d. [%]s [%]04d}, $mday, $mon_str, $year"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]d. [%]s [%]d}, $mday, $mon_str, $year"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d. [%]s}, $mday, $mon_str"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]s [%]s}, $mon_str, $year"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d.-[%]d. [%]s}, $sday, $eday, $smon_str"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d. [%]s-[%]d. [%]s}, $sday, $smon_str, $eday, $emon_str"
