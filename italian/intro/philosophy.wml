#use wml::debian::template title="La nostra filosofia: perché e come lo facciamo" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
	<ul class="toc">
		<li><a href="#freedom">La nostra missione: creare un Sistema
			Operativo Libero</a></li>
		<li><a href="#how">I nostri valori: come lavora la comunità</a>
	</ul>
</div>

<h2><a id="freedom">La nostra missione: creare un Sistema Operativo Libero</a></h2>

<p>
Il Progetto Debian è una associazione di persone che condividono uno scopo:
la creazione di un sistema operativo libero, disponibile gratuitamente per
tutti. Quando usiamo la parola <q>libero</q>, ci riferiamo alla
<em>libertà</em> del software.
</p>

<p style="text-align:center">
<button type="button"><span class="fas fa-unlock-alt fa-2x"></span>
<a href="free">La nostra definizione di software libero</a></button>
<button type="button"><span class="fas fa-book-open fa-2x"></span>
<a href="https://www.gnu.org/philosophy/free-sw">Cosa scrive la Free
Software Foundation sull'argomento</a></button>
</p>

<p>
Ci si può chiedere perché così tante persone hanno scelto di spendere
molte ore del proprio tempo a scrivere software, a metterlo insieme
attentamente e poi <em>darlo</em> tutto via senza chiedere nulla in
cambio? Le risposte sono molteplici, queste sono alcune:
</p>

<ul>
	<li>Ad alcune persone piace aiutare gli altri, partecipare a un
	progetto di software libero è un gran bel modo per raggiungere
	l'obiettivo.</li>

	<li>Molti sviluppatori scrivono programmi per imparare di più sui
	computer, sulle differenti architetture e i linguaggi di
	programmazione.</li>

	<li>Alcuni sviluppatori contribuiscono come modo per ringraziare per
	tutto il software che ricevono dagli altri.</li>

	<li>Nelle università molti creano software libero per condividere i
	risultati delle loro ricerche.</li>

	<li>Grandi aziende commerciali aiutano a mantenere il software libero
	per avere voce su come procede lo sviluppo oppure per ottenere
	rapidamente delle nuove funzionalità.</li>

	<li>Di sicuro, molti sviluppatori di Debian partecipano solo perché
	ritengono semplicemente che sia un enorme divertimento!</li>
</ul>

<p>
Sebbene il nostro credo sia il software libero, rispettiamo coloro che
a volte devono installare del software non-libero sulle loro macchine,
a prescindere dalla loro volontà di farlo. Abbiamo deciso di dare
supporto a questi utenti, quando possibile. C'è un numero crescente di
pacchetti che installano del software non-libero su un sistema Debian.
</p>

<aside>
<p>
<span class="fas fa-caret-right fa-3x"></span> Siamo dediti al software
libero e abbiamo formalizzato questa dedizione in un documento: il nostro
<a href="$(HOME)/social_contract">Contratto Sociale</a>
</p>
</aside>


<h2><a id="how">I nostri valori: come lavora la comunità</a></h2>

<p>
Il progetto Debian dispone di oltre mille <a
href="people">sviluppatori e collaboratori</a> attivi da <a
href="$(DEVEL)/developers.loc">tutto il mondo</a>. Un progetto
di questa dimensione ha bisogno di una <a
href="organization">struttura organizzativa</a>. Quindi, per chi si
chiede sia come funziona il progetto Debian e sia le regole e le
linee guida della comunità, può consultare i seguenti documenti:
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <ul>
        <li><a href="$(DEVEL)/constitution">Costituzione Debian</a>: <br>
			questo documento descrive la struttura organizzativa e spiega
			come il progetto Debian prende le decisioni formali.</li>

        <li><a href="../social_contract">Contratto Sociale e Linee Guida
			per il Software Libero</a>: <br>
			il Contratto Sociale Debian e le Linee Guida per il Software
			Libero (DFSG), che sono parte del contratto, descrivono la
			nostra dedizione al software libero e alla comunità del
			software libero.</li>
		  
        <li><a href="diversity">Dichiarazione sulla diversità</a>: <br>
			il Progetto Debian accoglie e incoraggia chiunque a partecipare,
			non importa come ci si identifica o come gli altri vi
			percepiscono.</li>
      </ul>
    </div>
  </div>

  <!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <ul>
        <li><a href="../code_of_conduct">Codice di condotta</a>: <br>
			abbiamo adottato un codice di condotta per i partecipanti
			alle nostre liste di messaggi, canali IRC, ecc.</li>
		  
        <li><a href="../doc/developers-reference/">Guida di riferimento
			per sviluppatori</a>: <br>
			questo documento fornisce una panoramica sulle procedure raccomandate
			e sulle risorse disponibili agli sviluppatori e ai manutentori
			Debian.</li>
		  
        <li><a href="../doc/debian-policy/">Debian Policy</a>: <br>
			un manuale che descrive i requisiti della distribuzione Debian,
			per esempio la struttura e i contenuti dell'archivio Debian, i
			requisiti tecnici che ogni pacchetto deve soddisfare per essere
			accettati, ecc.</li>
      </ul>
    </div>
  </div>

</div>

<p style="text-align:center">
<button type="button"><span class="fas fa-code fa-2x"></span> Debian
dall'interno: <a href="$(DEVEL)/">l'angolo degli sviluppatori</a></button>
</p>
