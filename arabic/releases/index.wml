#use wml::debian::template title="إصدارات دبيان"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ccf6e79eefe5b0ce09c41d5b657645e86214a9f3" maintainer="Med"

<p>
  لدبيان دائما ثلاث إصدارات على الأقل نشطة الدعم : <q>مستقرة</q> و  <q>اختبارية</q> و  <q>غير مستقرة</q>.
</p>

<dl>
<dt><a href="stable/">المستقرة</a></dt>
<dd>
<p>
  تتوفر التوزيعة <q>المستقرة</q> على آخر إصدار رسمي لدبيان.
</p>
<p>
  هذه هي إصدارة دبيان النهائية والتي ننصح باستخدامها في المقام الأول.
</p>
<p>

  الإصدارة <q>المستقرة</q> الحالية هي
<:=substr '<current_initial_release>', 0, 2:>، الاسم الرمزي <em><current_release_name></em>.
<ifeq "<current_initial_release>" "<current_release>"
  "صدرت بتاريخ <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
  "صدرت أوﻻ بصفة الإصدارة <current_initial_release>
  بتاريخ <current_initial_release_date> وآخر تحديث لها هو الإصدارة <current_release>، الصادرة بتاريخ <current_release_date>."
/>
</p>
</dd>

<dt><a href="testing/">الاختبارية</a></dt>
<dd>
<p>
  التوزيعة <q>الاختبارية</q> تتوفر على الحزم التي لم يتم قبولها بعد في الإصدارة <q>المستقرة</q>
  لكنها في قائمة الانتظار. الميزة الرئيسية لاستخدام هذه التوزيعة هي توفرها على إصدارات حديثة للحزم.
</p>
<p>
   لمزيد المعلومات <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">حول طبيعة <q>الاختبارية</q></a>
  و <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">كيف تصبح <q>مستقرة</q></a>
  راجع <a href="$(DOC)/manuals/debian-faq/">أسئلة دبيان الشائعة</a>.
</p>
<p>
  التوزيعة <q>الاختبارية</q> الحالية هي <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">غير المستقرة</a></dt>
<dd>
<p>
  التوزيعة <q>غير المستقرة</q> هي التي تتم عليها عملية التطوير. 
  تُستخدم هذه التوزيعة عمومًا من طرف المطوّرين ومحبي المغامرة.
  يُحبذ لمستخدمي غير المستقرة التسجيل في القائمة البريدية debian-devel-announce
  للتوصل بالتنبيهات المُهمة، مثل تنبيهات الترقيات التي من الممكن أن تكسر النظام.
</p>
<p>
<em>sid</em> هو الاسم الدائم للتوزيعة <q>غير المستقرة</q>.
</p>
</dd>
</dl>

<h2>دورة عيش الإصدارة</h2>
<p>
  يُعلن دبيان عن إصدارة توزيعه المستقرة بانتظام. يمكن للمستخدمين توقع
  ثلاث سنوات من الدعم الكامل لكل إصدارة بالإضافة لسنتين الدعم طويل الأمد (LTS).
</p>

<p>
  لمزيد من التفاصيل راجع صفحة
  <a href="https://wiki.debian.org/DebianReleases">إصدارات دبيان</a>
  وصفحة <a href="https://wiki.debian.org/LTS">دبيان LTS</a>
  في الويكي.
</p>

<h2>فهرس الإصدارات</h2>

<ul>

  <li><a href="<current_testing_name>/">الاسم الرمزي للإصدارة المستقرة القادمة هو 
    <q><current_testing_name></q></a>
      &mdash; لم يحدّد بعد أي تاريخ إصدار
  </li>

  <li><a href="buster/">دبيان 10 (<q>buster</q>)</a>
      &mdash; الإصدارة <q>المستقرة</q> الحالية
  </li>

  <li><a href="stretch/">دبيان 9 (<q>stretch</q>)</a>
      &mdash; إصدارة <q>مستقرة قديمة</q>، تحت <a href="https://wiki.debian.org/LTS">الدعم طويل الأمد (LTS)</a>
  </li>

  <li><a href="wheezy/">دبيان 8 (<q>jessie</q>)</a>
      &mdash; إصدارة <q>مستقرة جِد قديمة</q>،  تحت <a href="https://wiki.debian.org/LTS/Extended">الدعم طويل الأمد المُمدّد (ELTS)</a>
  </li>   

  <li><a href="wheezy/">دبيان 7 (<q>wheezy</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>     

  <li><a href="squeeze/">دبيان 6.0 (<q>squeeze</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>

  <li><a href="lenny/">دبيان جنو/لينكس 5.0 (<q>lenny</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>

  <li><a href="etch/">دبيان جنو/لينكس 4.0 (<q>etch</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>
  <li><a href="sarge/">دبيان جنو/لينكس 3.1 (<q>sarge</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>
  <li><a href="woody/">دبيان جنو/لينكس 3.0 (<q>woody</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>
  <li><a href="potato/">دبيان جنو/لينكس 2.2 (<q>potato</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>
  <li><a href="slink/">دبيان جنو/لينكس 2.1 (<q>slink</q>)</a> 
      &mdash; إصدارة مستقرة متروكة
  </li>
  <li><a href="hamm/">دبيان جنو/لينكس 2.0 (<q>hamm</q>)</a>
      &mdash; إصدارة مستقرة متروكة
  </li>
</ul>

<p>
صفحات وِب إصدارات دبيان المتروكة يتم حفظها، لكن الإصدارات نفسها
موجودة في <a href="$(HOME)/distrib/archive">أرشيف</a> مستقل.
</p>

<p>
راجع <a href="$(HOME)/doc/manuals/debian-faq/">أسئلة دبيان الشائعة</a> 
لتعرف <a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">مصدر هذه الاسماء الرمزية</a>.
</p>

<h2>سلامة البيانات في الإصدارات</h2>

<p>
سلامة البيانات مضمونة بواسطة ملف <code>Release</code>،
موقّع رقميًا. للتأكد من أن الملفات كافة تنتمي إلي نفس الإصدارة،
تدقيق مجموع (checksums) ملفات <code>Packages</code> كافة
منسوخ في الملف <code>Release</code>.
</p>

<p>
التوقيعات الرقمية لهذا الملف يتم تخزينها في الملف <code>Release.gpg</code> باستخدم 
النسخة الحالية من المفتاح المستخدم في توقيع الأرشيف. <q>للمستقرة</q> و <q>المستقرة القديمة</q>
هتاك توقيع إضافي خاص مولّد من طرف عضو من <a href="$(HOME)/intro/organization#release-team">\
فريق الإصدارة المستقرة </a> باستخدام مفتاح غير متّصل.
</p>
