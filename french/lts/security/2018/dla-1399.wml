#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux défauts ont été découverts dans ruby-passenger pour la prise en charge
de Ruby Rails et Rack qui permettaient à des attaquants d’usurper des en-têtes
HTTP ou exploiter une situation de compétition pour une élévation des privilèges
sous certaines conditions possibles.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7519">CVE-2015-7519</a>

<p>Des attaquants distants pourraient usurper des en-têtes passés à des
applications en utilisant un caractère de soulignement au lieu d’un tiret dans
un en-tête HTTP comme démontré avec un en-tête X_User.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12029">CVE-2018-12029</a>

<p>Une vulnérabilité a été découverte par l’équipe Pulse Security. Elle était
exploitable seulement lors de l’exécution de passenger_instance_registry_dir non
standard, à l'aide d'un situation de compétition où après un fichier était créé,
il existait une fenêtre dans laquelle il pouvait être remplacé par un lien
symbolique avant d’être « chowned » à l’aide du chemin et pas avec le
descripteur de fichier. Si la cible du lien symbolique était un fichier qui
devait être exécuté par root tel qu’un fichier crontab, alors une élévation
des privilèges était possible. Cela est maintenant limité par l’utilisation de
fchown().</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.0.53-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets ruby-passenger.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1399.data"
# $Id: $
