#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Depuis la publication de la dernière version stable de Debian (Stretch),
Debian LTS (Wheezy) a été renommé <q>oldoldstable</q>, ce qui casse le paquet
unattended-upgrades comme décrit dans le bogue n° 867169. Les mises à jour ne
seront tout simplement plus réalisées.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.79.5+wheezy3. Remarquez que la dernière version de
unattended-upgrades publiée dans les dernières versions n’a pas le même
comportement, car elle utilise le nom de code de la publication (par exemple,
<q>Jessie</q>) au lieu du nom de la suite (par exemple, <q>oldstable</q>) dans
le fichier de configuration. Aussi la transition des prochaines publications se
déroulera correctement pour les futures publications LTS.</p>

<p>Nous vous recommandons de mettre à jour vos paquets unattended-upgrades.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1032.data"
# $Id: $
