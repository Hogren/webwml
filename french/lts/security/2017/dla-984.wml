#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Tiff était affecté par plusieurs fuites de mémoire
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-9403">CVE-2017-9403</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9404">CVE-2017-9404</a>)
qui pourraient aboutir à un déni de service. En outre, alors que la version
actuelle dans Debian est déjà corrigée pour les problèmes de _TIFFVGetField
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-10095">CVE-2016-10095</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2017-9147">CVE-2017-9147</a>),
nous avons remplacé nos correctifs spécifiques par ceux fournis par l’amont pour
le suivre au plus près.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u14.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-984.data"
# $Id: $
