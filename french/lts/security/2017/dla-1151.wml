#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans wordpress, un outil de
blog web. Le projet « Common Vulnerabilities and Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9263">CVE-2016-9263</a>

<p>Quand l’exécution dans un bac à sable de flashmediaelement.swf
n’est pas utilisée, cela permet à des attaquants distants de mener des attaques
par injection Flash interdomaine (XSF) en exploitant du code contenu dans le
fichier wp-includes/js/mediaelement/flashmediaelement.swf.</p>

<p>Ce problème a été résolu par la suppression complète de flashmediaelement.swf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14718">CVE-2017-14718</a>

<p>WordPress était sujet à une attaque par script intersite dans
le modèle de lien à l'aide d'une URL javascript: ou data:.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14719">CVE-2017-14719</a>

<p>WordPress était vulnérable à une attaque par traversée de répertoires lors
d’opérations unzip dans les composants ZipArchive et PclZip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14720">CVE-2017-14720</a>

<p>WordPress permettait une attaque par script intersite dans la vue de liste
de modèles à l'aide d'un nom de modèle contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14721">CVE-2017-14721</a>

<p>WordPress permettait un script intersite dans l’éditeur de greffon à l'aide
d’un nom de greffon contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14722">CVE-2017-14722</a>

<p>WordPress permettait une attaque par traversée de répertoires dans le
composant Customizer à l'aide d'un nom de fichier de thème contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14723">CVE-2017-14723</a>

<p>WordPress gérait incorrectement les caractères % et les valeurs
supplémentaires de paramètres fictifs dans $wpdb->prepare, et par conséquent,
ne traitait pas correctement la possibilité de greffons et de thèmes d’établir
une attaque par injection SQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14725">CVE-2017-14725</a>

<p>WordPress était sujet à une attaque de redirection ouverte dans
wp-admin/user-edit.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14990">CVE-2017-14990</a>

<p>WordPress stocke en texte pur des valeurs de clef wp_signups.activation_key
(mais stocke les valeurs analogues wp_users.user_activation_key sous
forme de hachages), ce qui facilite pour des attaquants distants le détournement
de comptes inactifs d’utilisateurs en exploitant l’accès en lecture à la base
de données (tel qu’un accès obtenu à travers une vulnérabilité non précisée
d’injection SQL).</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.6.1+dfsg-1~deb7u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1151.data"
# $Id: $
