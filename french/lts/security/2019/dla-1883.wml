#use wml::debian::translation-check translation="b97e358244c5e7c8f054bc3accdd24061c1484f9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes mineurs ont été découverts dans tomcat8, un moteur de
servlets et JSP.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5388">CVE-2016-5388</a>

<p>Apache Tomcat, lorsque la servlet CGI est activée, suit la RFC 3875,
section 4.1.18 et par conséquent ne protège pas les applications contre la
présence de données de client non fiable dans la variable d’environnement
HTTP_PROXY. Cela pourrait permettre à des attaquants distants de rediriger le
trafic HTTP sortant vers un serveur mandataire arbitraire à l’aide d’un en-tête
contrefait de mandataire dans une requête HTTP, c'est-à-dire un problème
<q>httpoxy</q>. Le servlet <q>cgi</q> possède un paramètre <q>envHttpHeaders</q>
pour filtrer les variables d’environnement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8014">CVE-2018-8014</a>

<p>Les réglages par défaut pour le filtre CORS fourni dans Apache Tomcat sont
dangereux et activent <q>supportsCredentials</q> pour toutes les origines. Il
est attendu que les utilisateurs du filtre CORS l’auront configuré de manière
adéquate pour leur environnement plutôt que d’utiliser la configuration par
défaut. Par conséquent, il est supposé que la plupart des utilisateurs ne seront
pas impactés par ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0221">CVE-2019-0221</a>

<p>La commande SSI printenv dans Apache Tomcat répète les données fournies par
l’utilisateur sans les protéger et est, par conséquent, vulnérable à un script
intersite (XSS). SSI est désactivé par défaut. La commande printenv est destinée
au débogage et est probablement peu présente dans un site web en production.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8.0.14-1+deb8u15.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tomcat8.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1883.data"
# $Id: $
