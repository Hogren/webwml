#use wml::debian::translation-check translation="286f45dbd144ad438e554dc14e6c311e4e6ccbea" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités sont corrigées dans Asterisk, une boîte à outils au
code source ouvert pour autocommutateur téléphonique (PBX).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13161">CVE-2019-13161</a>

<p>Un attaquant était capable de planter Asterisk lors du traitement d’une
réponse SDP à une ré-invite T.38 sortante.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18610">CVE-2019-18610</a>

<p>Les utilisateurs d’AMI (Asterisk Manager Interface) distants authentifiés
sans autorisation du système pourraient exécuter des commandes système
arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18790">CVE-2019-18790</a>

<p>Vulnérabilité de détournement d’appel SIP.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:11.13.1~dfsg-2+deb8u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2017.data"
# $Id: $
