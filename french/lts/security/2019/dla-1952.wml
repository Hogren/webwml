#use wml::debian::translation-check translation="170189c786f9138b8a2fcf93f7ae8b973e096327" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux vulnérabilités ont été découvertes dans le démon de journalisation
rsyslog du système et noyau dans les analyseurs des messages de journaux AIX et
Cisco.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17041">CVE-2019-17041</a>

<p>Un problème a été découvert dans Rsyslog v. 8.1908.0.
La fonction contrib/pmaixforwardedfrom/pmaixforwardedfrom.c possède un dépassement de tas
dans l’analyseur pour les messages de journaux d’AIX. L’analyseur essaie de
trouver un délimiteur de message (dans ce cas, une espace ou un deux points)
mais échoue à estimer les chaînes ne satisfaisant pas cette contrainte. Si la
chaîne ne correspond pas, alors la variable lenMsg atteindra la valeur zéro et
évitera le test de validité qui détecte les messages non valables. Le message
sera considéré comme valable et l’analyseur gobera le délimiteur deux points
non existant. En faisant cela, il décrémentera lenMsg, un entier signé, dont la
valeur était zéro et deviendra alors moins un. L’étape suivante dans l’analyseur
est de décaler à gauche le contenu du message. Pour réaliser cela, il appellera
memmove avec les pointeurs corrects pour la cible et les chaînes de destination,
mais lenMsg sera alors interprété comme une valeur énorme, provoquant un
dépassement de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17042">CVE-2019-17042</a>

<p>Un problème a été découvert dans Rsyslog v. 8.1908.0.
La fonction contrib/pmcisconames/pmcisconames.c possède un dépassement de tas
dans l’analyseur pour les messages de journaux de Cisco. L’analyseur essaie de
trouver un délimiteur de message (dans ce cas, une espace ou un deux points)
mais échoue à estimer les chaînes ne satisfaisant pas cette contrainte. Si la
chaîne ne correspond pas, alors la variable lenMsg atteindra la valeur zéro et
évitera le test de validité qui détecte les messages non valables. Le message
sera considéré comme valable et l’analyseur gobera le délimiteur deux points
non existant. En faisant cela, il décrémentera lenMsg, un entier signé, dont la
valeur était zéro et deviendra alors moins un. L’étape suivante dans l’analyseur
est de décaler à gauche le contenu du message. Pour réaliser cela, il appellera
memmove avec les pointeurs corrects pour la cible et les chaînes de destination,
mais lenMsg sera alors interprété comme une valeur énorme, provoquant un
dépassement de tas.</p></li>


</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8.4.2-1+deb8u3.</p>
<p>Nous vous recommandons de mettre à jour vos paquets rsyslog.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1952.data"
# $Id: $
