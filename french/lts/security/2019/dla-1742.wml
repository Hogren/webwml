#use wml::debian::translation-check translation="14af97f244d50c6c3d0ec828022b40e92f2a2527" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Simon Scannell de Ripstech Technologies a découvert plusieurs
vulnérabilités dans wordpress, un gestionnaire de blog.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8942">CVE-2019-8942</a>

<p>Exécution de code à distance dans wordpress car l’entrée Post Meta
_wp_attached_file peut être changée en une chaîne arbitraire, telle qu’une avec
une sous-chaîne .jpg?file.php. Un attaquant avec les privilèges
d’auteur peut exécuter du code arbitraire en téléversant une image contrefaite
contenant du code PHP dans les métadonnées Exif.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9787">CVE-2019-9787</a>

<p>Wordpress ne filtre pas correctement le contenu des commentaires, conduisant
à l’exécution de code à distance par des utilisateurs non authentifiés dans la
configuration par défaut. Cela se produit car la protection CSRF est mal gérée
et parce que l’optimisation du moteur de recherche d’un élément est réalisée
incorrectement, conduisant à une vulnérabilité de script inter-site, celui-ci
procurant un accès administratif.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.1.26+dfsg-1+deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1742.data"
# $Id: $
