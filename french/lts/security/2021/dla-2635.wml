#use wml::debian::translation-check translation="bf25dbb3af13f84637d5d615ce7b4422968ceccb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans libspring-java, un
cadriciel modulaire d’applications Java/J2EE. Un attaquant peut exécuter du
code, réaliser des attaques XST, émettre des requêtes interdomaines non
autorisées ou causer un déni de service dans des configurations particulières.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1270">CVE-2018-1270</a>

<p>Le cadriciel Spring permet aux applications d’exposer STOMP à travers des
points terminaux WebSocket avec un simple courtier STOMP en mémoire à l’aide
du module spring-messaging. Un utilisateur malveillant (ou un attaquant) peut
contrefaire un message pour le courtier, pouvant conduire à une attaque distante
d’exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11039">CVE-2018-11039</a>

<p>Le cadriciel Spring permet à des applications web de modifier la méthode de
requête HTTP en n’importe quelle méthode HTTP (dont TRACE) en utilisant
HiddenHttpMethodFilter dans le MVC de Spring. Si une application possède déjà
une vulnérabilité de script intersite (XSS), un utilisateur malveillant (ou un
attaquant) peut utiliser ce filtre pour parvenir à une attaque XST (Cross Site
Tracing).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11040">CVE-2018-11040</a>

<p>Le cadriciel Spring permet aux applications web d’autoriser des requêtes
inter-domaines à l’aide de JSONP (JSON avec remplissage) à travers
AbstractJsonpResponseBodyAdvice pour les contrôleurs REST et
MappingJackson2JsonView pour les requêtes de courtier. Les deux ne sont pas
activées par défaut dans le cadriciel Spring comme dans Spring Boot, cependant,
quand MappingJackson2JsonView est configurée dans une application, la prise en
charge de JSONP est automatiquement prête à l’emploi à l’aide des
paramètres JSONP de <q>jsonp</q> et <q>callback</q>, autorisant les requêtes
interdomaines.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15756">CVE-2018-15756</a>

<p>Le cadriciel Spring fournit la prise en charge de requêtes d’intervalle
lors de service de ressources statiques à l’aide de ResourceHttpRequestHandler,
ou à partir de la version 5.0 quand un contrôleur annoté (métadonnées) renvoie
une org.springframework.core.io.Resource. Un utilisateur malveillant (ou un
attaquant) peut ajouter un en-tête d’intervalle avec un nombre élevé
d’intervalles, ou avec des intervalles vastes se chevauchant, ou à la fois,
pour une attaque par déni de service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.3.5-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libspring-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libspring-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libspring-java">\
https://security-tracker.debian.org/tracker/libspring-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2635.data"
# $Id: $
