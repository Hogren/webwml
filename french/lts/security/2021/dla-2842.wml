#use wml::debian::translation-check translation="0c5e030e87094573ce25a637433c792564b9a587" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Chen Zhaojun de l'équipe Alibaba Cloud Security a découvert que les
fonctions de JNDI, utilisés dans la configuration, les messages de journal
et les paramètres, ne protégeait pas d'un LDAP et d'autres points de
terminaison liés à JNDI contrôlés par un attaquant. Un attaquant qui peut
contrôler les messages ou les paramètres de messages de journal, peut
exécuter du code arbitraire chargé à partir de serveurs LDAP quand la
recherche/substitution de messages est active.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.7-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2842.data"
# $Id: $
