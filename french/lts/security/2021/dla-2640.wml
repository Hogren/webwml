#use wml::debian::translation-check translation="384dd1b883feef910fb3dc97e1618f06c688748e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d’utilisation de mémoire après libération a été découverte
dans le greffon Matroska du cadriciel multimédia GStreamer, qui pouvait aboutir
à un déni de service ou éventuellement à une exécution de code arbitraire si un
fichier multimédia mal formé était ouvert.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.10.4-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-good1.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gst-plugins-good1.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gst-plugins-good1.0">\
https://security-tracker.debian.org/tracker/gst-plugins-good1.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2640.data"
# $Id: $
