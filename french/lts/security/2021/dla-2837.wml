#use wml::debian::translation-check translation="626ea263634b54678822e48ac10a3369fcd9004f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans gmp, une bibliothèque
arithmétique à multi-précision de GNU. Il a été découvert qu'un dépassement
d'entier est possible dans mpz/inp_raw.c ainsi qu'un dépassement de tampon
en résultant au moyen d'une entrée contrefaite, menant à une erreur de
segmentation sur les plateformes 32 bits.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2:6.1.2+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gmp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gmp, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gmp">\
https://security-tracker.debian.org/tracker/gmp</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2837.data"
# $Id: $
