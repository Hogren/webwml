#use wml::debian::translation-check translation="e8839f8806fa14dd94a06d8a066b32e849711504" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans Wireshark, un analyseur de
réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13619">CVE-2019-13619</a>

<p>Plantage de disséqueurs BER et relatifs ASN.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16319">CVE-2019-16319</a>

<p>Le disséqueur Gryphon pourrait entrer dans une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19553">CVE-2019-19553</a>

<p>Le disséqueur CMS pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7045">CVE-2020-7045</a>

<p>Le disséqueur BT ATT pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9428">CVE-2020-9428</a>

<p>Le disséqueur EAP pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9430">CVE-2020-9430</a>

<p>Le disséqueur DLMAP WiMax pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9431">CVE-2020-9431</a>

<p>Le disséqueur RRC LTE pourrait faire fuiter la mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11647">CVE-2020-11647</a>

<p>Le disséqueur BACapp pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13164">CVE-2020-13164</a>

<p>Le disséqueur NFS pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15466">CVE-2020-15466</a>

<p>Le disséqueur GVCP pourrait entrer dans une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25862">CVE-2020-25862</a>

<p>Le disséqueur TCP pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25863">CVE-2020-25863</a>

<p>Le disséqueur MIME Multipart pourrait planter.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26418">CVE-2020-26418</a>

<p>Fuite de mémoire dans le disséqueur de protocole Kafka.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26421">CVE-2020-26421</a>

<p>Plantage dans le disséqueur de protocole USB HID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26575">CVE-2020-26575</a>

<p>Le protocole Zero de Facebook (alias FBZERO) pourrait entrer dans une boucle
infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28030">CVE-2020-28030</a>

<p>Le disséqueur GQUIC pourrait planter.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.6.20-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wireshark.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wireshark, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2547.data"
# $Id: $
