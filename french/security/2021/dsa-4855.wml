#use wml::debian::translation-check translation="0b9c89566ef13daffbc9185681dac5de2ae0d592" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans OpenSSL, une boîte à
outils SSL (Secure Socket Layer). Un bogue de débordement dans la procédure
x64_64 d’exponentiation rapide de Montgomery (« Montgomery squaring »), un
dépassement d'entier dans CipherUpdate et un défaut de déréférencement de
pointeur NULL de X509_issuer_and_serial_hash() pourraient avoir pour
conséquence un déni de service.</p>

<p>Pour plus de détails, consultez les avertissements amont :"
<a href="https://www.openssl.org/news/secadv/20191206.txt">\
https://www.openssl.org/news/secadv/20191206.txt</a> et
<a href="https://www.openssl.org/news/secadv/20210216.txt">\
https://www.openssl.org/news/secadv/20210216.txt</a>.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1.1.1d-0+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openssl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openssl">\
https://security-tracker.debian.org/tracker/openssl</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4855.data"
# $Id: $
