#use wml::debian::translation-check translation="d5484a72e71b8da89fe25f18d5d9248fab4b56c6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour fournit un microcode à jour pour certains types de
processeurs Intel ainsi que des atténuations de vulnérabilités de sécurité
qui pourraient avoir pour conséquences une élévation de privilèges en
combinaison avec VT-d et diverses attaques par canal auxiliaire.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés dans
la version 3.20210608.2~deb10u1.</p>

<p>Notez que deux régressions ont été signalées ; pour certains processeurs
CoffeeLake la mise à jour peut casser iwlwifi
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/56)
et avec certains processeurs Skylake R0/D0 sur des machines utilisant un
micrologiciel ou un BIOS très dépassé, le système peut bloquer au démarrage :
(https://github.com/intel/Intel-Linux-Processor-Microcode-Data-Files/issues/31)</p>

<p>Si vous êtes affectés par un de ces problèmes, vous pouvez récupérer
votre système en désactivant le chargement du microcode au démarrage (comme
cela est documenté dans README.Debian, aussi disponible en ligne à l'adresse
<a href="https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian">\
https://salsa.debian.org/hmh/intel-microcode/-/blob/master/debian/README.Debian)</a></p>

<p>Nous vous recommandons de mettre à jour vos paquets intel-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de intel-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/intel-microcode">\
https://security-tracker.debian.org/tracker/intel-microcode</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4934.data"
# $Id: $
