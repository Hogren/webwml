#use wml::debian::translation-check translation="9c86c2cbe8b155155d63f34caca3d7d55c073157" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans le serveur HTTP Apache :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44224">CVE-2021-44224</a>

<p>Lorsqu'il opèrait comme mandataire de réacheminement, Apache dépendait
d'une configuration vulnérable à un déni de service ou une contrefaçon de
requête côté serveur (SSRF).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44790">CVE-2021-44790</a>

<p>Un dépassement de tampon dans mod_lua pouvait avoir pour conséquences un
déni de service ou éventuellement l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 2.4.38-3+deb10u7.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.4.52-1~deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5035.data"
# $Id: $
