#use wml::debian::template title="Informações de lançamento do Debian &ldquo;etch&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/etch/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="552f9232b5d467c677963540a5a4236c824e87b9"


<p>O Debian GNU/Linux <current_release_etch> foi
lançado em <a href="$(HOME)/News/<current_release_newsurl_etch/>"><current_release_date_etch></a>.
O Debian 4.0 foi lançado inicialmente em <:=spokendate('2007-04-08'):>.
O lançamento incluiu várias grandes mudanças, descritas em
nosso <a href="$(HOME)/News/2007/20070408">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian GNU/Linux 4.0 foi substituído pelo
<a href="../lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>.
Atualizações de segurança foram descontinuadas no final de Fevereiro de 2010.
</strong></p>

<p>Para obter e instalar o Debian GNU/Linux, consulte
a página de <a href="debian-installer/">informações de instalação</a>
e o <a href="installmanual">manual de instalação</a>. Para atualizar
de uma versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Apesar de nossos desejos, podem existir alguns problemas nesta versão,
embora ela tenha sido declarada estável (stable). Nós fizemos
<a href="errata">uma lista dos problemas conhecidos mais importantes</a>,
e você sempre pode <a href="reportingbugs">relatar outros problemas</a>
para nós.</p>

<p>Por último mas não menos importante, nós temos uma lista de <a href="credits">pessoas
que recebem crédito</a> por fazerem este lançamento acontecer.</p>
