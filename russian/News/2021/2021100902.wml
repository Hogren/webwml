#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 10: выпуск 10.11</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает об одиннадцатом обновлении своего
предыдущего стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Исправления различных ошибок</h2>

<p>Данное обновление предыдущего стабильного выпуска вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction atftp "Исправление переполнения буфера [CVE-2021-41054]">
<correction base-files "Обновление для редакции 10.11">
<correction btrbk "Исправление выполнения произвольного кода [CVE-2021-38173]">
<correction clamav "Новый стабильный выпуск основной ветки разработки; исправление ошибок сегментирования clamdscan при использовании --fdpass и --multipass вместе с ExcludePath">
<correction commons-io "Исправление обхода пути [CVE-2021-29425]">
<correction cyrus-imapd "Исправление отказа в обслуживании [CVE-2021-33582]">
<correction debconf "Проверка, что whiptail или dialog можно использовать">
<correction debian-installer "Повторная сборка с учётом buster-proposed-updates; обновление ABI Linux до версии 4.19.0-18">
<correction debian-installer-netboot-images "Повторная сборка с учётом buster-proposed-updates">
<correction distcc "Исправление кросскомпиляторных ссылок GCC в update-distcc-symlinks и добавление поддержки для clang и CUDA (nvcc)">
<correction distro-info-data "Обновление поставляемых данных о нескольких выпусках">
<correction dwarf-fortress "Удаление нераспространяемых предварительно собранных библиотек из tar-архива с исходным кодом">
<correction espeak-ng "Исправление использования espeak с mbrola-fr4, если mbrola-fr1 не установлен">
<correction gcc-mingw-w64 "Исправление обработки gcov">
<correction gthumb "Исправление переполнения динамической памяти [CVE-2019-20326]">
<correction hg-git "Исправление ошибок тестирования с новыми версиями git">
<correction htslib "Исправление autopkgtest на i386">
<correction http-parser "Исправление подделки HTTP-запросов [CVE-2019-15605]">
<correction irssi "Исправление использования указателей после освобождения памяти при SASL-входе на сервер [CVE-2019-13045]">
<correction java-atk-wrapper "Использование dbus для обнаружения включения режима специальных возможностей">
<correction krb5 "Исправление разыменования null-указателя в KDC при FAST-запросе с пустым сервером [CVE-2021-37750]; исправление утечки памяти в krb5_gss_inquire_cred">
<correction libdatetime-timezone-perl "Новый стабильный выпуск основной ветки разработки; обновление DST-правил для Самоа и Иордании; подтверждение отсутствия корректировочной секунды 2021-12-31">
<correction libpam-tacplus "Предотвращение добавления разделяемых секретов в виде обычного текста в системный журнал [CVE-2020-13881]">
<correction linux "<q>proc: отслеживание /proc/$pid/attr/ opener mm_struct</q>, исправляющее проблемы с lxc-attach; новый стабильный выпуск основной ветки разработки; повышение версии ABI до 18; [rt] обновление до версии 4.19.207-rt88; usb: hso: исправление ошибки обработки кода hso_create_net_device [CVE-2021-37159]">
<correction linux-latest "Обновление до ABI версии 4.19.0-18">
<correction linux-signed-amd64 "<q>proc: отслеживание /proc/$pid/attr/ opener mm_struct</q>, исправляющее проблемы с lxc-attach; новый стабильный выпуск основной ветки разработки; повышение версии ABI до 18; [rt] обновление до версии 4.19.207-rt88; usb: hso: исправление ошибки обработки кода hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc: отслеживание /proc/$pid/attr/ opener mm_struct</q>, исправляющее проблемы с lxc-attach; новый стабильный выпуск основной ветки разработки; повышение версии ABI до 18; [rt] обновление до версии 4.19.207-rt88; usb: hso: исправление ошибки обработки кода hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc: отслеживание /proc/$pid/attr/ opener mm_struct</q>, исправляющее проблемы с lxc-attach; новый стабильный выпуск основной ветки разработки; повышение версии ABI до 18; [rt] обновление до версии 4.19.207-rt88; usb: hso: исправление ошибки обработки кода hso_create_net_device [CVE-2021-37159]">
<correction mariadb-10.3 "Новый стабильный выпуск основной ветки разработки; исправления безопасности [CVE-2021-2389 CVE-2021-2372]; исправление пути к исполняемому файлу Perl в сценариях">
<correction modsecurity-crs "Исправление проблемы с пропуском тела запроса [CVE-2021-35368]">
<correction node-ansi-regex "Исправление отказа в обслуживании из-за регулярного выражения [CVE-2021-3807]">
<correction node-axios "Исправление отказа в обслуживании из-за регулярного выражения [CVE-2021-3749]">
<correction node-jszip "Использование пустого прототипа объекта для this.files [CVE-2021-23413]">
<correction node-tar "Удаление не являющихся каталогами путей из кэша каталогов [CVE-2021-32803]; более полная очистка абсолютных путей [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Исправление установки NVVMIR_LIBRARY_DIR на ppc64el">
<correction nvidia-graphics-drivers "Новый стабильный выпуск основной ветки разработки; исправление отказов в обслуживании [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-driver-libs: добавление поля Recommends: libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "Новый стабильный выпуск основной ветки разработки; исправление отказов в обслуживании [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095]; nvidia-legacy-390xx-driver-libs: добавление поля Recommends: libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "Новый стабильный выпуск основной ветки разработки; исправление неправильного планирования повторяемого применения шага проекции [CVE-2021-3677]; более полный запрет повторного согласования SSL">
<correction proftpd-dfsg "Исправление ошибок <q>Утечки содержимого памяти в mod_radius к серверу radius</q>, <q>невозможно отключить повторное согласование для FTPS, инициированное клиентом</q>, вход в каталоги по символьным ссылкам, аварийная остановка mod_sftp при использовании pubkey-auth с ключами DSA">
<correction psmisc "Исправление регрессии в killall, при которой нельзя выбрать процесс с именем более 15 символов">
<correction python-uflash "Обновление URL прошивки">
<correction request-tracker4 "Исправление атаки по таймингу во время входа [CVE-2021-38562]">
<correction ring "Исправление отказа в обслуживании в поставляемой копии pjproject [CVE-2021-21375]">
<correction sabnzbdplus "Предотвращение выхода из каталога в функции renamer [CVE-2021-29488]">
<correction shim "Добавление заплаты для arm64 для настройки разметки секции и прекращения аварийных остановок; в небезопасном режиме не отменять действие, если не удалётся создать переменную MokListXRT; не отменять действие при ошибках установки grub; вместо этого выводить предупреждение">
<correction shim-helpers-amd64-signed "Добавление заплаты для arm64 для настройки разметки секции и прекращения аварийных остановок; в небезопасном режиме не отменять действие, если не удаётся создать переменную MokListXRT; не отменять действие при ошибках установки grub; вместо этого выводить предупреждение">
<correction shim-helpers-arm64-signed "Добавление заплаты для arm64 для настройки разметки секции и прекращения аварийных остановок; в небезопасном режиме не отменять действие, если не удаётся создать переменную MokListXRT; не отменять действие при ошибках установки grub; вместо этого выводить предупреждение">
<correction shim-helpers-i386-signed "Добавление заплаты для arm64 для настройки разметки секции и прекращения аварийных остановок; в небезопасном режиме не отменять действие, если не удаётся создать переменную MokListXRT; не отменять действие при ошибках установки grub; вместо этого выводить предупреждение">
<correction shim-signed "Временное решение проблем с поломкой загрузки на arm64 путём включения более старой рабочей версии неподписанного загрузчика shim на указанной платформе; переход на arm64 обратно на использование текущей неподписанной сборки; добавление заплаты для arm64 для настройки разметки секции и прекращения аварийных остановок; в небезопасном режиме не отменять действие, если не удаётся создать переменную MokListXRT; не отменять действие при ошибках установки grub; вместо этого выводить предупреждение">
<correction shiro "Исправление обхода аутентификации [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510]; обновление заплаты совместимости со Spring Framework; поддержка Guice 4">
<correction tzdata "Обновление DST-правил для Самоа и Иордании; подтверждение отсутствия корректировочной секунды 2021-12-31">
<correction ublock-origin "Новый стабильный выпуск основной ветки разработки; исправление отказа в обслуживании [CVE-2021-36773]">
<correction ulfius "Проверка того, что память инициализирована перед её использованием [CVE-2021-40540]">
<correction xmlgraphics-commons "Исправление подделки запросов на стороне сервера [CVE-2020-11988]">
<correction yubikey-manager "Добавление отсутствующей зависимости от python3-pkg-resources в пакет yubikey-manager">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction birdtray "Несовместим с новыми версиями Thunderbird">
<correction libprotocol-acme-perl "Поддерживает только ACME устаревшей версии 1">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию предыдущего стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий предыдущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>Предлагаемые обновления для предыдущего стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Информация о предыдущем стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
