# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 01:54+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Πωλητής"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Επιτρέπει συνεισφορές"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Αρχιτεκτονικές"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Διεθνείς αποστολές εμπορευμάτων"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Επαφή"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Έδρα του Πωλητή"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "σελίδα"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "email"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "Ευρωπαΐκές αποστολές"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Σε ορισμένες περιοχές"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "πηγαίος κώδικας"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "και"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Επιτρέπει συνεισφορά στο Debian:"

#~ msgid "Architectures:"
#~ msgstr "Αρχιτεκτονικές:"

#~ msgid "CD Type:"
#~ msgstr "Είδος CD:"

#~ msgid "Country:"
#~ msgstr "Χώρα:"

#~ msgid "Custom Release"
#~ msgstr "Προσαρμοσμένη Έκδοση"

#~ msgid "DVD Type:"
#~ msgstr "Είδος DVD:"

#~ msgid "Development Snapshot"
#~ msgstr "Στιγμιότυπο Ανάπτυξης"

#~ msgid "Multiple Distribution"
#~ msgstr "Πολλαπλές Διανομές"

#~ msgid "Official CD"
#~ msgstr "Επίσημο CD"

#~ msgid "Official DVD"
#~ msgstr "Επίσημο DVD"

#~ msgid "Ship International:"
#~ msgstr "Διεθνείς αποστολές:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Ιστοσελίδα για το Debian:"

#~ msgid "Vendor Release"
#~ msgstr "Έκδοση Πωλητή"

#~ msgid "Vendor:"
#~ msgstr "Πωλητής:"

#~ msgid "contrib included"
#~ msgstr "Περιλαμβάνει το contrib"

#~ msgid "email:"
#~ msgstr "email:"

#~ msgid "non-US included"
#~ msgstr "Περιλαμβάνει το non-US"

#~ msgid "non-free included"
#~ msgstr "Περιλαμβάνει το non-free"

#~ msgid "reseller"
#~ msgstr "μεταπωλητής"

#~ msgid "reseller of $var"
#~ msgstr "μεταπωλητής του $var"

#~ msgid "updated monthly"
#~ msgstr "Ανανεώνεται μηνιαία"

#~ msgid "updated twice weekly"
#~ msgstr "Ανανεώνεται δυο φορές την εβδομάδα"

#~ msgid "updated weekly"
#~ msgstr "Ανανεώνεται καθε εβδομάδα"

#~ msgid "vendor additions"
#~ msgstr "Με προσθήκες απο τον πωλητή"
