#use wml::debian::template title="Debian 10 -- Παροράματα" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Γνωστά σφάλματα</toc-add-entry>
<toc-add-entry name="security">Ζητήματα ασφαλείας</toc-add-entry>

<p>Η ομάδα ασφαλείας του Debian εκδίδει επικαιροποιημένα πακέτα για τη
σταθερή διανομή για τα οποία έχει ταυτοποιήσει προβλήματα σχετιζόμενα
με την ασφάλεια. Παρακαλούμε συμβουλευτείτε τις 
<a href="$(HOME)/security/">Σελίδες ασφαλείας</a> για πληροφορίες σχετικά
με ζητήματα ασφαλείας που έχουν εντοπιστεί στην έκδοση <q>buster</q>.</p>

<p>Αν χρησιμοποιείτε το APT, προσθέστε την παρακάτω γραμμή  στο αρχείο 
<tt>/etc/apt/sources.list</tt>
για να μπορέσετε να έχετε πρόσβαση στις τελευταίες αναβαθμίσεις ασφαλείας:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>Μετά από αυτό, εκτελέστε την εντολή <kbd>apt update</kbd> και στη συνέχεια 
την εντολή <kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Ενδιάμεσες (Point) εκδόσεις</toc-add-entry>

<p>Μερικές φορές, στην περίπτωση αρκετών κρίσιμης σημασίας προβλημάτων 
ασφαλείας, η διανομή που έχει κυκλοφορήσει επικαιροποιείται. Γενικά, αυτές οι
επικαιροποιήσεις χαρακτηρίζονται ως "ενδιάμεσες" (point) εκδόσεις.</p>

<!-- <ul>
  <li>The first point release, 9.1, was released on
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_buster> 10.0 "

<p>Δεν υπάρχουν ακόμα ενδιάμεσες εκδόσεις για το Debian 10.</p>" "

<p>Δείτε τη σελίδα  <a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
ChangeLog</a> για λεπτομέρειες σχετικά με τις αλλαγές ανάμεσα στην έκδοση 10 
και την τρέχουσα έκδοση <current_release_buster/>.</p>"/>


<p>Οι διορθώσεις στην σταθερή έκδοση που είναι σε κυκλοφορία περνάνε συχνά 
από μια εκτεταμένη χρονική περίοδο δοκιμών πριν γίνουν δεκτές στην αρχειοθήκη.
Παρ' όλα αυτά, αυτές οι διορθώσεις είναι διαθέσιμες στον κατάλογο
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> οποιούδήποτε καθρέφτη της αρχειοθήκης του 
Debian.</p>

<p>Αν χρησιμοποιείτε το APT για την αναβάθμιση των πακέτων σας, μπορείτε να 
εγκαταστήσετε τις προτεινόμενες αναβαθμίσεις προσθέτοντας την παρακάτω γραμμή 
στο αρχείο
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 10 point release
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>Μετά από αυτό, εκτελέστε την εντολή <kbd>apt update</kbd> και στη συνέχεια 
την εντολή
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Σύστημα εγκατάστασης</toc-add-entry>

<p>
Για πληροφορίες σχετικά με σφάλματα και αναβαθμίσεις για το σύστημα 
εγκατάστασης, δείτε τη σελίδα <a 
href="debian-installer/">πληροφορίες εγκατάστασης</a>.
</p>
